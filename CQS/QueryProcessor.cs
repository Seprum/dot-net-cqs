﻿namespace CQS
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Contracts.Queries;

    public abstract class QueryProcessor: IQueryProcessor, IQueryProcessorAsync
    {
        private readonly Dictionary<Type, Func<IQuery, object>> _handlerLocator;

        /// <inheritdoc />
        protected QueryProcessor()
        {
            _handlerLocator = new Dictionary<Type, Func<IQuery, object>>();
        }

        /// <inheritdoc />
        public TResult Process<TResult>(IQuery<TResult> query)
        {
            return (TResult)_handlerLocator[query.GetType()].Invoke(query);
        }

        /// <inheritdoc />
        public Task<TResult> ProcessAsync<TResult>(IQuery<TResult> query)
        {
            return Task.Run(() => Process(query));
        }

        /// <summary>
        ///     Registers a new query handler
        /// </summary>
        /// <typeparam name="TQuery">A query type</typeparam>
        /// <typeparam name="TResult">A query result type</typeparam>
        /// <typeparam name="THandler">A query handler type</typeparam>
        /// <param name="handler">A query handler to be registered within current processor</param>
        protected void RegisterHandler<TQuery, TResult, THandler>(THandler handler)
            where TQuery: class, IQuery<TResult>
            where THandler: IQueryHandler<TQuery, TResult>
        {
            _handlerLocator.Add(typeof(TQuery), query => handler.Handle(query as TQuery));
        }
    }
}
