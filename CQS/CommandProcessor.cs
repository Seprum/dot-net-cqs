﻿namespace CQS
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Contracts.Commands;

    /// <inheritdoc />
    public abstract class CommandProcessor: ICommandProcessor, ICommandProcessorAsync
    {
        private readonly Dictionary<Type, Func<ICommand, bool>> _handlerLocator;

        /// <inheritdoc />
        protected CommandProcessor()
        {
            _handlerLocator = new Dictionary<Type, Func<ICommand, bool>>();
        }

        /// <inheritdoc />
        public bool Process(ICommand command)
        {
            return _handlerLocator[command.GetType()].Invoke(command);
        }

        /// <inheritdoc />
        public Task<bool> ProcessAsync(ICommand command)
        {
            return Task.Run(() => Process(command));
        }

        /// <summary>
        ///     Registers a new command handler
        /// </summary>
        /// <typeparam name="TCommand">A command type</typeparam>
        /// <typeparam name="THandler">A command handler type</typeparam>
        /// <param name="handler">A command handler to be registered within current processor</param>
        protected void RegisterHandler<TCommand, THandler>(THandler handler)
            where TCommand: class, ICommand
            where THandler: ICommandHandler<TCommand>
        {
            _handlerLocator.Add(typeof(TCommand), command => handler.Handle(command as TCommand));
        }
    }
}
