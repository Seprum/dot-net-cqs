﻿namespace CQS.Contracts.Queries
{
    /// <summary>
    ///     Represents a handler for queries execution
    /// </summary>
    /// <typeparam name="TQuery">A query type</typeparam>
    /// <typeparam name="TResult">A query result type</typeparam>
    public interface IQueryHandler<in TQuery, out TResult> where TQuery: IQuery<TResult>
    {
        /// <summary>
        ///     Executes particular query
        /// </summary>
        /// <param name="query">A query object</param>
        /// <returns>A query result</returns>
        TResult Handle(TQuery query);
    }
}
