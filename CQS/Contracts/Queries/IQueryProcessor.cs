﻿namespace CQS.Contracts.Queries
{
    /// <summary>
    ///     Defines a processor for processing queries
    /// </summary>
    public interface IQueryProcessor
    {
        /// <summary>
        ///     Processes a query using appropriate query handler
        /// </summary>
        /// <typeparam name="TResult">A query result type</typeparam>
        /// <param name="query">A query object</param>
        /// <returns>A query result</returns>
        TResult Process<TResult>(IQuery<TResult> query);
    }
}
