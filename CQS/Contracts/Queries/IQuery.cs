﻿namespace CQS.Contracts.Queries
{
    /// <summary>
    ///     Represents a query object
    /// </summary>
    public interface IQuery
    {
    }

    /// <summary>
    ///     Represents a query object
    /// </summary>
    /// <typeparam name="TResult">A query result type</typeparam>
    public interface IQuery<TResult>: IQuery
    {
    }
}
