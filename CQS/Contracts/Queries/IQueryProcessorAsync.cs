﻿namespace CQS.Contracts.Queries
{
    using System.Threading.Tasks;

    /// <summary>
    ///     Defines an asynchronous processor for processing queries
    /// </summary>
    public interface IQueryProcessorAsync
    {
        /// <summary>
        ///     Processes a query asynchronously using appropriate query handler
        /// </summary>
        /// <typeparam name="TResult">A query result type</typeparam>
        /// <param name="query">A query object</param>
        /// <returns>A query result</returns>
        Task<TResult> ProcessAsync<TResult>(IQuery<TResult> query);
    }
}
