﻿namespace CQS.Contracts.Commands
{
    /// <summary>
    ///     Represents a command object
    /// </summary>
    public interface ICommand
    {
    }
}
