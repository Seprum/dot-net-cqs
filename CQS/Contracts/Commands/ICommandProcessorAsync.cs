﻿namespace CQS.Contracts.Commands
{
    using System.Threading.Tasks;

    /// <summary>
    ///     Defines an asynchronous processor for processing commands
    /// </summary>
    public interface ICommandProcessorAsync
    {
        /// <summary>
        ///     Processes a command asynchronously using appropriate command handler
        /// </summary>
        /// <param name="command">A command object</param>
        /// <returns>A command execution result</returns>
        Task<bool> ProcessAsync(ICommand command);
    }
}
