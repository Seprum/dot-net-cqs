﻿namespace CQS.Contracts.Commands
{
    /// <summary>
    ///     Defines a processor for processing commands
    /// </summary>
    public interface ICommandProcessor
    {
        /// <summary>
        ///     Processes a command using appropriate command handler
        /// </summary>
        /// <param name="command">A command object</param>
        /// <returns>A command execution result</returns>
        bool Process(ICommand command);
    }
}
