﻿namespace CQS.Contracts.Commands
{
    /// <summary>
    ///     Represents a handler for commands execution
    /// </summary>
    /// <typeparam name="TCommand">A command type</typeparam>
    public interface ICommandHandler<in TCommand> where TCommand: ICommand
    {
        /// <summary>
        ///     Executes particular command
        /// </summary>
        /// <param name="command">A command object</param>
        /// <returns>A command execution result</returns>
        bool Handle(TCommand command);
    }
}
