﻿namespace CQS.Example.WriteModel
{
    using CommandHandlers;
    using Commands;

    internal class TodoCommandProcessor: CommandProcessor
    {
        public TodoCommandProcessor(DataSource dataSource)
        {
            var todoCommandHandler = new TodoCommandHandler(dataSource);

            RegisterHandler<CreateTodoCommand, TodoCommandHandler>(todoCommandHandler);
            RegisterHandler<SetTodoStateCommand, TodoCommandHandler>(todoCommandHandler);
            RegisterHandler<RemoveTodoCommand, TodoCommandHandler>(todoCommandHandler);
        }
    }
}
