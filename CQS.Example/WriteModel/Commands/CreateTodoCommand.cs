﻿namespace CQS.Example.WriteModel.Commands
{
    using Contracts.Commands;

    internal class CreateTodoCommand: ICommand
    {
        public string Description { get; set; }
    }
}
