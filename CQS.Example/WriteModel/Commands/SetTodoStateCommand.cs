﻿namespace CQS.Example.WriteModel.Commands
{
    using System;
    using Contracts.Commands;

    internal class SetTodoStateCommand: ICommand
    {
        public Guid Id { get; set; }

        public bool Done { get; set; }
    }
}
