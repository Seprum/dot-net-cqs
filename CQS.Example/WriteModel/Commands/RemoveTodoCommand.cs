﻿namespace CQS.Example.WriteModel.Commands
{
    using System;
    using Contracts.Commands;

    internal class RemoveTodoCommand: ICommand
    {
        public Guid Id { get; set; }
    }
}
