﻿namespace CQS.Example.WriteModel.CommandHandlers
{
    using System;
    using System.Linq;
    using Commands;
    using Contracts.Commands;
    using Models;

    internal class TodoCommandHandler: ICommandHandler<CreateTodoCommand>,
        ICommandHandler<SetTodoStateCommand>,
        ICommandHandler<RemoveTodoCommand>
    {
        private readonly DataSource _dataSource;

        public TodoCommandHandler(DataSource dataSource)
        {
            _dataSource = dataSource;
        }

        public bool Handle(CreateTodoCommand command)
        {
            _dataSource.Add(new Todo
            {
                Id = Guid.NewGuid(),
                Description = command.Description,
                Done = false
            });

            return true;
        }

        public bool Handle(SetTodoStateCommand command)
        {
            var todo = _dataSource.FirstOrDefault(t => t.Id == command.Id);

            if (todo == null)
            {
                return false;
            }

            todo.Done = command.Done;

            return true;
        }

        public bool Handle(RemoveTodoCommand command)
        {
            var todo = _dataSource.FirstOrDefault(t => t.Id == command.Id);

            if (todo == null)
            {
                return false;
            }

            _dataSource.Remove(todo);

            return true;
        }
    }
}
