﻿namespace CQS.Example
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Models;
    using ReadModel;
    using ReadModel.Queries;
    using WriteModel;
    using WriteModel.Commands;

    internal class Program
    {
        public static void Main(string[] args)
        {
            var dataSource = new DataSource();
            var queryProcessor = new TodoQueryProcessor(dataSource);
            var commandProcessor = new TodoCommandProcessor(dataSource);

            commandProcessor.Process(new CreateTodoCommand
            {
                Description = "Create a backend"
            });

            commandProcessor.Process(new CreateTodoCommand
            {
                Description = "Write an application"
            });

            commandProcessor.Process(new CreateTodoCommand
            {
                Description = "Conquer the world"
            });

            var todos = queryProcessor.Process(new TodosQuery());

            PrintTodos(todos);

            commandProcessor.Process(new SetTodoStateCommand
            {
                Id = todos.First().Id,
                Done = true
            });

            commandProcessor.Process(new SetTodoStateCommand
            {
                Id = todos.ElementAt(1).Id,
                Done = true
            });

            commandProcessor.Process(new RemoveTodoCommand
            {
                Id = todos.Last().Id
            });

            var modifiedTodos = queryProcessor.Process(new TodosQuery());

            PrintTodos(modifiedTodos);

            var initialTodo = queryProcessor.Process(new TodoQuery
            {
                Id = todos.First().Id
            });

            PrintTodo(initialTodo);

            Console.ReadKey();
        }

        private static void PrintTodos(ICollection<Todo> todos)
        {
            Console.WriteLine("Todo list:");
            if (todos.Any())
            {
                foreach (var todo in todos)
                {
                    PrintTodo(todo);
                }
            }
            else
            {
                Console.WriteLine("Empty.");
            }
            Console.WriteLine();
        }

        private static void PrintTodo(Todo todo)
        {
            var state = todo.Done ? "Done" : "Undone";
            Console.WriteLine($"Description: {todo.Description} ({state})");
        }
    }
}
