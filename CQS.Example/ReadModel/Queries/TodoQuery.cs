﻿namespace CQS.Example.ReadModel.Queries
{
    using System;
    using Models;
    using Contracts.Queries;

    internal class TodoQuery : IQuery<Todo>
    {
        public Guid Id { get; set; }
    }
}
