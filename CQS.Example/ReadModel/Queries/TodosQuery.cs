﻿namespace CQS.Example.ReadModel.Queries
{
    using System.Collections.Generic;
    using Models;
    using Contracts.Queries;

    internal class TodosQuery : IQuery<ICollection<Todo>>
    {
        public bool? Done { get; set; }
    }
}
