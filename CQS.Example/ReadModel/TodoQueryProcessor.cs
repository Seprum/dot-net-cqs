﻿namespace CQS.Example.ReadModel
{
    using System.Collections.Generic;
    using Models;
    using Queries;
    using QueryHandlers;

    internal class TodoQueryProcessor: QueryProcessor
    {
        public TodoQueryProcessor(DataSource dataSource)
        {
            var todoQueryHandler = new TodoQueryHandler(dataSource);

            RegisterHandler<TodoQuery, Todo, TodoQueryHandler>(todoQueryHandler);
            RegisterHandler<TodosQuery, ICollection<Todo>, TodoQueryHandler>(todoQueryHandler);
        }
    }
}
