﻿namespace CQS.Example.ReadModel.QueryHandlers
{
    using System.Collections.Generic;
    using System.Linq;
    using Contracts.Queries;
    using Models;
    using Queries;

    internal class TodoQueryHandler: IQueryHandler<TodoQuery, Todo>, IQueryHandler<TodosQuery, ICollection<Todo>>
    {
        private readonly DataSource _dataSource;

        public TodoQueryHandler(DataSource dataSource)
        {
            _dataSource = dataSource;
        }

        public Todo Handle(TodoQuery query)
        {
            return _dataSource.FirstOrDefault(t => t.Id == query.Id);
        }

        public ICollection<Todo> Handle(TodosQuery query)
        {
            return query.Done.HasValue
                ? _dataSource.Where(t => t.Done == query.Done.Value).ToList()
                : _dataSource.ToList();
        }
    }
}
