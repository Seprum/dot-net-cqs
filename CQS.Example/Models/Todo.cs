﻿namespace CQS.Example.Models
{
    using System;

    internal class Todo
    {
        public Guid Id { get; set; }

        public string Description { get; set; }

        public bool Done { get; set; }
    }
}
